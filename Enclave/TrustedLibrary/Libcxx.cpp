#include "../Enclave.h"
#include "Enclave_t.h"

void ecall_demo(int argc, char **argv, int *a, int *b, int *c)
{
    int m, n, k;
    if (argc > 3) {
        m = atoi(argv[1]);
        k = atoi(argv[2]);
        n = atoi(argv[3]);
    } else {
        m = 2;
        k = 4096;
        n = 11008;
    }
    printf("m = %d, k = %d, n = %d\n", m, k, n);

    get_time_start();
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            for (int kk = 0; kk < k; ++kk) {
                c[i * n + j] += a[i * k + kk] * b[kk * n + j];
            }
        }
    }
    get_time_end();

    for (int p = 0; p < 32; ++p) {
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                c[i * n + j] = 0;
            }
        }
        get_time_start();
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                for (int kk = 0; kk < k; ++kk) {
                    c[i * n + j] += a[i * k + kk] * b[kk * n + j];
                }
            }
        }
        get_time_end();
    }

    // for (int i = 0; i < m; ++i) {
    //     for (int j = 0 ; j < n; ++j) {
    //         printf("%d ", c[i * n + j]);
    //     }
    //     printf("\n");
    // }
}