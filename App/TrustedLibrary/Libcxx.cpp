#include "../App.h"
#include "Enclave_u.h"

void ecall_libcxx_functions(int argc, char **argv, int *a, int *b, int *c)
{
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;

    ret = ecall_demo(global_eid, argc, argv, a, b, c);

    if (ret != SGX_SUCCESS)
        abort();
}